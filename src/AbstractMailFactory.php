<?php

namespace Kisphp;

abstract class AbstractMailFactory
{
    abstract public function createMailConfig();

    public static function createMailer()
    {
        $factory = new static();
        $config = $factory->createMailConfig();

        return $factory->createMessenger($config);
    }

    public function createMessenger(MailConfigInterface $config)
    {
        return new Messenger($config);
    }
}
