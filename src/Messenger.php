<?php

namespace Kisphp;

class Messenger implements MessengerInterface
{
    const MAIL_MESSAGE_TYPE = 'text/html';

    /**
     * @var \Swift_Transport
     */
    protected $transport;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Swift_Message
     */
    protected $message;

    /**
     * @var MailConfigInterface
     */
    protected $mailConfig;

    /**
     * @param MailConfigInterface $mailConfig
     */
    public function __construct($mailConfig)
    {
        $this->mailConfig = $mailConfig;

        $this->createMailTransport();
        $this->createMailer();
    }

    /**
     * @param array $arrayEmailName
     * @param string $subject
     * @param string $htmlMessage
     *
     * @return MessengerInterface
     */
    public function createMailMessage(array $arrayEmailName, $subject, $htmlMessage)
    {
        $this->message = new \Swift_Message($subject);
        $this->message->setFrom([
            $this->mailConfig->getFromEmail() => $this->mailConfig->getFromName(),
        ]);
        $this->message->setTo($arrayEmailName);
        $this->message->setBody($htmlMessage, static::MAIL_MESSAGE_TYPE);

        return $this;
    }

    /**
     * @return int
     * @throws \Swift_DependencyException
     */
    public function send()
    {
        if ($this->message === null) {
            throw new \Swift_DependencyException('You must access method "createMailMessage" first');
        }

        return $this->mailer->send($this->message);
    }

    protected function createMailTransport()
    {
        $this->transport = new \Swift_SmtpTransport(
            $this->mailConfig->getHost(),
            $this->mailConfig->getPort(),
            $this->mailConfig->getMailEncryptionType()
        );

        $this->transport
            ->setUsername($this->mailConfig->getSenderUsername())
            ->setPassword($this->mailConfig->getSenderPassword())
        ;
    }

    protected function createMailer()
    {
        $this->mailer = new \Swift_Mailer($this->transport);
    }
}
