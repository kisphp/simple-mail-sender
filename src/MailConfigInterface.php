<?php

namespace Kisphp;

interface MailConfigInterface
{
    public function getHost();

    public function getPort();

    public function getSenderUsername();

    public function getSenderPassword();

    public function getMailEncryptionType();

    public function getFromEmail();

    public function getFromName();
}
