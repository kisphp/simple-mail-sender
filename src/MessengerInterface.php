<?php

namespace Kisphp;

interface MessengerInterface
{
    public function createMailMessage(array $arrayEmailName, $subject, $htmlMessage);

    public function send();
}
